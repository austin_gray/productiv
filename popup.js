/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;
}

document.addEventListener('DOMContentLoaded', function() {
  getCurrentTabUrl(function(url) {
    renderStatus('Current site: ' + url);
  });
  document.getElementById("block-site").addEventListener("click", function() {
    getCurrentTabUrl(function(url) {
      var rObj = extractDomain(url);
      var basename = rObj.domain;
      var protocol = rObj.protocol;
      if (protocol.indexOf('chrome') > -1) {
        renderStatus('Cannot block Chrome settings pages');
        return false;
      }
      if (basename.indexOf("juliabalfour") > -1) {
        renderStatus('Cannot block: ' + basename);
        return false;
      }
      var updated = [];
      chrome.storage.sync.get('siteblockr_banned_urls', function(obj) {
        var blocked_urls = obj.siteblockr_banned_urls;
        if (typeof blocked_urls != 'undefined' && blocked_urls.length > 0) {
          blocked_urls.push(basename);
          updated = blocked_urls;
        } else {
          updated.push(basename);
        }
        chrome.storage.sync.set({'siteblockr_banned_urls': updated}, function() {
          renderStatus('Added ' + basename + ' to your block list.');
        });
      });
    });
  });
});

function extractDomain(url) {
    var domain;
    var protocol;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
        protocol = url.split('/')[0];
    }
    else {
        domain = url.split('/')[0];
        protocol = '';
    }
    //find & remove port number
    domain = domain.split(':')[0];
    rObj = {};
    rObj.domain = domain;
    rObj.protocol = protocol;
    return rObj;
}
