(function() {

	var waterfallBlockedSites = function() {

		chrome.storage.sync.get('siteblockr_banned_urls', function(obj) {
		
			var blocked_urls = obj.siteblockr_banned_urls;
			var list = document.getElementById('sites');
			while( list.firstChild ){
			  list.removeChild( list.firstChild );
			}
	    
	    for (var i = 0;  i < blocked_urls.length; i++) {
				var blocked_url = blocked_urls[i];
				var li = document.createElement('li');
				li.appendChild(document.createTextNode(blocked_url));
				
				var a = document.createElement('a');
				a.appendChild(document.createTextNode('remove'));
				a.setAttribute('data-url', blocked_url);
				a.setAttribute('class', 'unblock');

				a.addEventListener("click", removeHandler, false);

				li.appendChild(a);
				list.appendChild(li);
			}

		});

	};

	var removeHandler = function() {

		var urlToRemove = this.getAttribute('data-url');

		chrome.storage.sync.get('siteblockr_banned_urls', function(obj) {
      var blocked_urls = obj.siteblockr_banned_urls;
      if (blocked_urls.length < 1) {
      	alert('cannot remove what does not exist');
        return false;
      } else {
      	var index = blocked_urls.indexOf(urlToRemove);
      	if (index > -1) {
      		blocked_urls.splice(index, 1);
      		chrome.storage.sync.set({'siteblockr_banned_urls': blocked_urls}, function() {
		      	waterfallBlockedSites();
		      });
      	}
      }
    });
	};

	var initTaskHandler = function() {

		chrome.storage.sync.get('current_task', function(obj) {
			var current_task = obj.current_task;
			document.getElementById("current-task").value = current_task;
		});

		var a = document.getElementById("save-task");
		a.addEventListener("click", function() {
			var updated_task = document.getElementById("current-task").value;
			chrome.storage.sync.set({current_task: updated_task}, function() {
				alert('task updated');
			});
		});

	};


	var init = function() {
		waterfallBlockedSites();
		initTaskHandler();
	};
	init();

})();