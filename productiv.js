const isEmpty = (task) => {
	if (task === '') {
		return true;
	}

	if (task === 'undefined') {
		return true;
	}

	if (typeof task === 'undefined') {
		return true;
	}

	return false;
}

(function() {

	var blocked = false;

	var current_url = window.location.href;

	chrome.storage.sync.get('siteblockr_banned_urls', function(obj) {

    var blocked_urls = obj.siteblockr_banned_urls;

		if (typeof blocked_urls != 'undefined') {

			for (var i = 0;  i < blocked_urls.length; i++) {
				var blocked_url = blocked_urls[i];
				if (current_url.indexOf(blocked_url) > -1) {
					blocked = true;
				}
			}

		}

		if (blocked) {
			window.location.replace("http://stackoverflow.com");
		}

  });

	window.onload = function() {

		chrome.storage.sync.get('current_task', function(obj) {
	  	var current_task = obj.current_task;
			if (!isEmpty(current_task)) {
				var b = document.createElement('b');
				b.appendChild(document.createTextNode('Current Task: '));
				var el = document.createElement('div');
				el.appendChild(b);
				el.appendChild(document.createTextNode(current_task));
				el.setAttribute('style', 'position:fixed; z-index:9999999; bottom:0; left:0; background:#fff; color:#000; padding:7px; display:inline-block; border:1px solid red;');
				document.body.parentNode.appendChild(el);
			}
	  });

	};

})();
